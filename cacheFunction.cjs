function cacheFunction(callback){

    if(typeof(callback)!="function"){
        throw new Error("please give a valid function as a parameter called callback function ");
    }
      
    let cache ={};
    function returnFunction(...args){
    // stringArgument=String(args);
        
        const argInString = JSON.stringify(args)
        if(argInString in cache){
            // console.log(cache)
            return cache[argInString];
        }
        else{
            cache[argInString]=callback(...args);
            // console.log(cache[argInString]); 
            return cache[argInString]; 
        }
    
    
    } 
return returnFunction;
}
module.exports =cacheFunction;