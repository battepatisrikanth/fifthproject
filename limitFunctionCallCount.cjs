function limitFunctionCallCount(callback,numberLimit){
    if(typeof(numberLimit)!="number" || typeof(callback)!="function" || numberLimit<0){
        throw new Error("this is an error");
    }
    let returnFunction=function(...args){
        if(numberLimit>0 ){
            numberLimit--;
            return callback(...args);
            
        }
        return null;
    }
    return returnFunction;
}

module.exports=limitFunctionCallCount;